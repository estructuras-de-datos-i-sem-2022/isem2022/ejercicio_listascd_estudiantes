/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Estudiante;
import util.ArchivoLeerURL;
import util.colecciones_seed.ListaCD;

/**
 *
 * @author profesor
 */
public class SIA {

    private ListaCD<Estudiante> estudiantes = new ListaCD();

    public SIA() {
    }

    public SIA(String url) {

        ArchivoLeerURL f = new ArchivoLeerURL(url);
        Object datos[] = f.leerArchivo();
        for (int i = 1; i < datos.length; i++) {
            //Convierto el object a String
            String fila = datos[i].toString();
            //Particiono cada fila con el split
            //codigos;nombre de estudiante;email;semestre
            String dato_estudiante[] = fila.split(";");
            int cod = Integer.parseInt(dato_estudiante[0]);
            String nombre = dato_estudiante[1];
            String email = dato_estudiante[2];
            byte sem = Byte.parseByte(dato_estudiante[3]);
            //Creo al estudiante:
            Estudiante nuevo = new Estudiante(cod, nombre, email, sem);
            //Inserto al estudiante en la lista:
            this.estudiantes.insertarFin(nuevo);

        }

    }

    @Override
    public String toString() {

        String msg = "";
        //Uso el iterador de la clase ListaCD:
        for (Estudiante x : this.estudiantes) {
            msg += x.toString() + "\n";
        }
        return msg;
    }

    /**
     * Obtiene el(los) semestre(s) que contienen la mayor cantidad de estudiantes.
     * la cadena generada estará separada por ",", ejemplo: "1,7"-> significa que el primer y el séptimo semestre contiene la mayor cantidad de estudiantes
     * @return 
     */
    public String getSem_Mas_estudiantes()
    {
        return ":)";
    }
}
